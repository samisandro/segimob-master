namespace SigImobBack.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderSale",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdSale = c.Int(nullable: false),
                        IdProduct = c.Int(nullable: false),
                        Quantity = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.IdProduct, cascadeDelete: true)
                .ForeignKey("dbo.Sale", t => t.IdSale, cascadeDelete: true)
                .Index(t => t.IdSale)
                .Index(t => t.IdProduct);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        Value = c.Single(nullable: false),
                        Product_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.Product_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.Sale",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateSale = c.DateTime(nullable: false),
                        CommissionValue = c.Single(nullable: false),
                        TotalValue = c.Single(nullable: false),
                        IdSeller = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Seller", t => t.IdSeller, cascadeDelete: true)
                .Index(t => t.IdSeller);
            
            CreateTable(
                "dbo.Seller",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        DocumentId = c.String(nullable: false, maxLength: 13),
                        Salary = c.Single(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        Gender = c.String(nullable: false, maxLength: 1),
                        Address = c.String(nullable: false, maxLength: 200),
                        Seller_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Seller", t => t.Seller_Id)
                .Index(t => t.Seller_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderSale", "IdSale", "dbo.Sale");
            DropForeignKey("dbo.Sale", "IdSeller", "dbo.Seller");
            DropForeignKey("dbo.Seller", "Seller_Id", "dbo.Seller");
            DropForeignKey("dbo.OrderSale", "IdProduct", "dbo.Product");
            DropForeignKey("dbo.Product", "Product_Id", "dbo.Product");
            DropIndex("dbo.Seller", new[] { "Seller_Id" });
            DropIndex("dbo.Sale", new[] { "IdSeller" });
            DropIndex("dbo.Product", new[] { "Product_Id" });
            DropIndex("dbo.OrderSale", new[] { "IdProduct" });
            DropIndex("dbo.OrderSale", new[] { "IdSale" });
            DropTable("dbo.Seller");
            DropTable("dbo.Sale");
            DropTable("dbo.Product");
            DropTable("dbo.OrderSale");
        }
    }
}
