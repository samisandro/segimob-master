﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SigImobBack.Models
{
    public class OrderSale
    {
        [Key]
        public int Id { get; set; }
        public int IdSale { get; set; }
        public int IdProduct { get; set; }
        [Required]
        public float Quantity { get; set; }

        [ForeignKey("IdSale")]
        public Sale Sale { get; set; }

        [ForeignKey("IdProduct")]
        public Product Product { get; set; }

    }
}