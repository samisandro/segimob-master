﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SigImobBack.Models
{
    public class Sale
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime DateSale { get; set; }
        [Required]
        public float CommissionValue { get; set; }
        [Required]
        public float TotalValue { get; set; }
        public int IdSeller { get; set; }

        [ForeignKey("IdSeller")]
        public Seller Seller { get; set; }

    }
}