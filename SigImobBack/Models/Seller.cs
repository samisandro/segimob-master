﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SigImobBack.Models
{
    public class Seller
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(150)]
        public string Name { get; set; }
        [Required]
        [MaxLength(13)]
        public string DocumentId { get; set; }
        [Required]
        public float Salary { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        [Required]
        [MaxLength(1)]
        public string Gender { get; set; }
        [Required]
        [MaxLength(200)]
        public string Address { get; set; }
        public virtual ICollection<Seller> ListSeller { get; set; }
    }
}