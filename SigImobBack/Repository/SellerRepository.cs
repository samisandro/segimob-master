﻿using SigImobBack.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SigImobBack.Repository
{
    public class SellerRepository : BaseRepository
    {
        private Seller sl;
        public Seller GetById(int Id)
        {
            return db.Seller.FirstOrDefault(x => x.Id == Id);
        }

        public List<Seller> GetAll()
        {
            return db.Seller.OrderBy(x => x.Name).ToList();
        }

        public List<Seller> GetByName(string Name)
        {
            return db.Seller.Where(x => x.Name.Contains(Name)).OrderBy(x => x.Name).ToList();
        }

        public void Save(Seller entity)
        {
            db.Seller.Add(entity);
            db.SaveChanges();
        }

        public void Update(int Id, Seller entity)
        {
            sl = this.GetById(Id);
            sl.Name = entity.Name;
            sl.DocumentId = entity.DocumentId;
            sl.Salary = entity.Salary;
            sl.BirthDate = entity.BirthDate;
            sl.Gender = entity.Gender;
            sl.Address = entity.Address;
            db.Entry(sl).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(Seller entity)
        {
            db.Seller.Remove(entity);
            db.SaveChanges();
        }
    }
}