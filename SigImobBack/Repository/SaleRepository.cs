﻿using SigImobBack.DTO;
using SigImobBack.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SigImobBack.Repository
{
    public class SaleRepository : BaseRepository
    {
        private Sale s;
        private SellerRepository _SellerRepository;
        public SellerRepository SellerRepository
        {
            get
            {
                if (_SellerRepository == null)
                    _SellerRepository = new SellerRepository();
                return _SellerRepository;
            }
            set { _SellerRepository = value; }
        }

        private static readonly Expression<Func<Sale, SaleDTO>> AsSaleDTO =
           x => new SaleDTO
           {
               Id = x.Id,
               IdSeller = x.IdSeller,
               DateSale = x.DateSale,
               CommissionValue = x.CommissionValue,
               TotalValue = x.TotalValue,
               NameSeller = x.Seller.Name
           };

        public SaleDTO GetOne(int id)
        {
            SaleDTO sale = db.Sale.Include(s => s.Seller)
                    .Where(s => s.Id == id)
                    .Select(AsSaleDTO)
                    .FirstOrDefault();
            return sale;
        }
        public List<Sale> GetByIdSeller(int id)
        {
            return db.Sale.Where(x => x.IdSeller == id).ToList();
        }
        public IQueryable<SaleDTO> GetDTO()
        {
            return db.Sale.Include(s => s.Seller).Select(AsSaleDTO);
        }
        public Sale GetById(int Id)
        {
            return db.Sale.FirstOrDefault(x => x.Id == Id);
        }

        public List<Sale> GetAll()
        {
            return db.Sale.OrderBy(x => x.DateSale).ToList();
        }

        public void Save(Sale entity)
        {
            db.Sale.Add(entity);
            db.SaveChanges();
        }

        public void Update(int Id, Sale entity)
        {
            s = this.GetById(Id);
            s.DateSale = entity.DateSale;
            s.CommissionValue = entity.CommissionValue;
            s.TotalValue = entity.TotalValue;
            s.IdSeller = entity.IdSeller;            
            db.Entry(s).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(Sale entity)
        {
            db.Sale.Remove(entity);
            db.SaveChanges();
        }
    }
}