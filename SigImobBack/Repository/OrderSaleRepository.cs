﻿using SigImobBack.DTO;
using SigImobBack.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SigImobBack.Repository
{
    public class OrderSaleRepository : BaseRepository
    {
        private ProductRepository _ProductRepository;
        private SaleRepository _SaleRepository;
        private OrderSale os;

        public SaleRepository SaleRepository
        {
            get
            {
                if (_SaleRepository == null)
                    _SaleRepository = new SaleRepository();
                return _SaleRepository;
            }
            set { _SaleRepository = value; }
        }
        public ProductRepository ProductRepository
        {
            get
            {
                if (_ProductRepository == null)
                    _ProductRepository = new ProductRepository();
                return _ProductRepository;
            }
            set { _ProductRepository = value; }
        }

        //private static readonly Expression<Func<OrderSale, OrderSaleDTO>> AsOrderSaleDTO =
        //    x => new OrderSaleDTO
        //    {
        //        Id = x.Id,
        //        IdSale = x.IdSale,
        //        IdProduct = x.IdProduct,
        //        Quantity = x.Quantity,
        //        NameProduct = x.Product.Name
        //    };
        //public OrderSaleDTO GetOne(int id)
        //{
        //    OrderSaleDTO orderSale = db.OrderSale.Include(p => p.Product)
        //            .Where(p => p.Id == id)
        //            .Select(AsOrderSaleDTO)
        //            .FirstOrDefault();
        //    return orderSale;
        //}

        //public IQueryable<OrderSaleDTO> GetDTO()
        //{
        //    return db.OrderSale.Include(p => p.Product).Select(AsOrderSaleDTO);
        //}

        public OrderSale GetById(int Id)
        {
            return db.OrderSale.FirstOrDefault(x => x.Id == Id);
        }
        public List<OrderSale> GetAll()
        {
            return db.OrderSale.ToList();
        }
        public void Save(OrderSale entity)
        {
            db.OrderSale.Add(entity);
            db.SaveChanges();
        }
        public void Update(int Id, OrderSale entity)
        {
            os = this.GetById(Id);
            os.IdProduct = entity.IdProduct;
            os.IdSale = entity.IdSale;
            os.Quantity = entity.Quantity;           
            db.Entry(os).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void Delete(OrderSale entity)
        {
            db.OrderSale.Remove(entity);
            db.SaveChanges();
        }


    }
}