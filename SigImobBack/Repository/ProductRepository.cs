﻿using SigImobBack.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SigImobBack.Repository
{
    public class ProductRepository : BaseRepository
    {
        private Product p;
        public Product GetById(int Id)
        {
            return db.Product.FirstOrDefault(x => x.Id == Id);
        }

        public List<Product> GetAll()
        {
            return db.Product.OrderBy(x => x.Name).ToList();
        }

        public List<Product> GetByName(string Name)
        {
            return db.Product.Where(x => x.Name.Contains(Name)).OrderBy(x => x.Name).ToList();
        }

        public void Save(Product entity)
        {
            db.Product.Add(entity);
            db.SaveChanges();
        }

        public void Update(int Id, Product entity)
        {
            p = this.GetById(Id);
            p.Name = entity.Name;
            p.Value = entity.Value;
            db.Entry(p).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(Product entity)
        {
            db.Product.Remove(entity);
            db.SaveChanges();
        }
    }
}