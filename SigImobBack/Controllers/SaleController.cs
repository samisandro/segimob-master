﻿using Newtonsoft.Json;
using SigImobBack.DTO;
using SigImobBack.Models;
using SigImobBack.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;


namespace SigImobBack.Controllers
{
    [EnableCors("*", "*", "*")]
    public class SaleController : ApiController
    {
        private SaleRepository _SaleRepository;
        private Sale entity;
        public SaleRepository SaleRepository
        {
            get
            {
                if (_SaleRepository == null)
                    _SaleRepository = new SaleRepository();
                return _SaleRepository;
            }
            set { _SaleRepository = value; }
        }
        //public HttpResponseMessage Get()
        //{
        //    try
        //    {
        //        var response = new HttpResponseMessage(HttpStatusCode.OK);
        //        response.Content = new StringContent(JsonConvert.SerializeObject(SaleRepository.GetAll()));
        //        return response;
        //    }
        //    catch
        //    {
        //        return new HttpResponseMessage(HttpStatusCode.BadRequest);
        //    }
        //}

        public IQueryable<SaleDTO> Get()
        {
            IQueryable<SaleDTO> list = SaleRepository.GetDTO();
            return list;
        }
        public SaleDTO Get(int id, Sale s)
        {
            return SaleRepository.GetOne(id);
        }

        public HttpResponseMessage Get(int Id)
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(SaleRepository.GetById(Id)));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Post(Sale s)
        {
            try
            {
                SaleRepository.Save(s);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Put(int Id, Sale s)
        {
            try
            {
                SaleRepository.Update(Id, s);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Delete(int Id)
        {
            try
            {
                entity = SaleRepository.GetById(Id);
                SaleRepository.Delete(entity);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}