﻿using Newtonsoft.Json;
using SigImobBack.DTO;
using SigImobBack.Models;
using SigImobBack.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace SigImobBack.Controllers
{
    [EnableCors("*", "*", "*")]
    public class OrderSaleController : ApiController
    {
        private OrderSaleRepository _OrderSaleRepository;
        private OrderSale entity;
        public OrderSaleRepository OrderSaleRepository
        {
            get
            {
                if (_OrderSaleRepository == null)
                    _OrderSaleRepository = new OrderSaleRepository();
                return _OrderSaleRepository;
            }
            set { _OrderSaleRepository = value; }
        }
        public HttpResponseMessage Get()
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(OrderSaleRepository.GetAll()));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
        //public IQueryable<OrderSaleDTO> GetDTO()
        //{
        //    IQueryable<OrderSaleDTO> list = OrderSaleRepository.GetDTO();
        //    return list;
        //}
        //public SaleDTO Get(int id, Sale s)
        //{
        //    return OrderSaleRepository.GetOne(id);
        //}
        public HttpResponseMessage Get(int Id)
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(OrderSaleRepository.GetById(Id)));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Post(OrderSale os)
        {
            try
            {
                OrderSaleRepository.Save(os);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Put(int Id, OrderSale os)
        {
            try
            {
                OrderSaleRepository.Update(Id, os);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Delete(int Id)
        {
            try
            {
                entity = OrderSaleRepository.GetById(Id);
                OrderSaleRepository.Delete(entity);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}