﻿using Newtonsoft.Json;
using SigImobBack.Models;
using SigImobBack.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;


namespace SigImobBack.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ProductController : ApiController
    {
        private ProductRepository _ProductRepository;
        private Product entity;
        public ProductRepository ProductRepository
        {
            get
            {
                if (_ProductRepository == null)
                    _ProductRepository = new ProductRepository();
                return _ProductRepository;
            }
            set { _ProductRepository = value; }
        }
        public HttpResponseMessage Get()
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(ProductRepository.GetAll()));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Get(int Id)
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(ProductRepository.GetById(Id)));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Post(Product p)
        {
            try
            {
                ProductRepository.Save(p);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Put(int Id, Product p)
        {
            try
            {
                ProductRepository.Update(Id, p);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Delete(int Id)
        {
            try
            {
                entity = ProductRepository.GetById(Id);
                ProductRepository.Delete(entity);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}