﻿using Newtonsoft.Json;
using SigImobBack.Models;
using SigImobBack.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;


namespace SigImobBack.Controllers
{
    [EnableCors("*", "*", "*")]
    public class SellerController : ApiController
    {
        private SellerRepository _SellerRepository;
        private Seller entity;
        public SellerRepository SellerRepository
        {
            get
            {
                if (_SellerRepository == null)
                    _SellerRepository = new SellerRepository();
                return _SellerRepository;
            }
            set { _SellerRepository = value; }
        }
        public HttpResponseMessage Get()
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(SellerRepository.GetAll()));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Get(int Id)
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(SellerRepository.GetById(Id)));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Post(Seller sl)
        {
            try
            {
                SellerRepository.Save(sl);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Put(int Id, Seller sl)
        {
            try
            {
                SellerRepository.Update(Id, sl);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Delete(int Id)
        {
            try
            {
                entity = SellerRepository.GetById(Id);
                SellerRepository.Delete(entity);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}