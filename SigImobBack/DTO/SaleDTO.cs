﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SigImobBack.DTO
{
    public class SaleDTO
    {
        public int Id { get; set; }        
        public DateTime DateSale { get; set; }        
        public float CommissionValue { get; set; }       
        public float TotalValue { get; set; }
        public int IdSeller { get; set; }
        public string NameSeller { get; set; }
    }
}